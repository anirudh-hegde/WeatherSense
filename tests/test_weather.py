import pytest
from weather_app import app
from flask import Flask, request, render_template

@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()
    yield client


def test_weather_route(client):
    response = client.get('/')
    assert response.status_code == 200


# def test_weather_with_city(client):
#     response = client.post('/', data={'city': 'New York'})
#     assert b"The weather in New York is:" in response.data
#
#
# def test_weather_no_city(client):
#     response = client.post('/')
#     assert b"No city found" in response.data
#
#
# def test_weather_invalid_city(client):
#     response = client.post('/', data={'city': 'NonexistentCity123'})
#     assert b"No city found" in response.data
